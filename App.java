import models.Cat;
import models.Dog;
import models.BigDog;
public class App {
    public static void main(String[] args) throws Exception {
        Cat cat = new Cat("Kitty");
        Dog dog = new Dog("Corgi");
        BigDog bigdong = new BigDog("BullDong");

        System.out.println(cat.toString());
        System.out.println(dog.toString());
        System.out.println(bigdong.toString());

        cat.greets();
        dog.greets();
        dog.greets(dog);
        bigdong.greets();
        bigdong.greets(dog);
        bigdong.greets(bigdong);
    }
}
